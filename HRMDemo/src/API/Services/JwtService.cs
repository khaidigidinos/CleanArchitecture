﻿using Application.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace API.Services
{
    public class JwtService : IJwtService
    {
        private readonly IConfiguration _configuration;
        private static string Id = "id";
        private static string Name = "name";
        private static string Email = "email";
        private static string Role = "role";
        public JwtService(IConfiguration configuration) 
        {
            _configuration = configuration;
        }
        public string GenerateJwtToken(string id, string username, string useremail, string userrole)
        {
            var jwtOptions = _configuration.GetSection("Jwt");
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions["SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim> 
            {
                new Claim(JwtService.Id, id),
                new Claim(JwtService.Name, username),
                new Claim(JwtService.Email, useremail),
                new Claim(JwtService.Role, userrole)
            };

            var token = new JwtSecurityToken(jwtOptions["Issuer"],
              jwtOptions["Audience"],
              claims,
              expires: DateTime.Now.AddDays(Convert.ToDouble(jwtOptions["LifeTime"])),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
