﻿using API.Services;
using Application.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace API.DependencyInjection
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddAPIDependencyInjection(this IServiceCollection services) 
        {
            /* Register services */
            services.AddSingleton<IJwtService, JwtService>();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            return services;
        }
    }
}
