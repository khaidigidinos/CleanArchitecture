﻿using Application.Common.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.Controllers
{
    public abstract class ApiBaseController : ControllerBase
    {
        private ICurrentUserService _currentUserService;
        private IConfiguration _configuration;
        private ISender _mediatR;

        protected ICurrentUserService CurrentUserService => _currentUserService ??= HttpContext.RequestServices.GetService<ICurrentUserService>();
        protected IConfiguration Configuration => _configuration ??= HttpContext.RequestServices.GetService<IConfiguration>();
        protected ISender MediatR => _mediatR ??= HttpContext.RequestServices.GetService<ISender>();
    }
}
