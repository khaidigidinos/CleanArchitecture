﻿using Application.Common.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("v1/test")]
    public class TestController : ApiBaseController
    {
        private IJwtService _jwtService;
        protected IJwtService JwtService => _jwtService ??= HttpContext.RequestServices.GetService<IJwtService>();

        [HttpGet]
        public IActionResult GetUserInfo()
        {
            var claims = CurrentUserService.GetCurrentUserClaims();
            var arr = new string[claims.Count];
            for (int i = 0; i < arr.Length; i++) 
            {
                arr.SetValue(claims[i].Value, i);
            }
            return Ok(arr);
        }

        [HttpPost]
        public async Task<string> GetJwtToken(string userName, string userEmail) 
        {
            return JwtService.GenerateJwtToken(Guid.NewGuid().ToString(), "khai.nm", "khai29012001@gmail.com", "User");
        }
    }
}
