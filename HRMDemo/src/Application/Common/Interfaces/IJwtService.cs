﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Interfaces
{
    public interface IJwtService
    {
        public string GenerateJwtToken(string id, string username, string useremail, string userrole);
    }
}
