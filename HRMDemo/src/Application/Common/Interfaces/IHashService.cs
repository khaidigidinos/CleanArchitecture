﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Interfaces
{
    public interface IHashService
    {
        public string StringToSHA256(string hashNeeded);
    }
}
