﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Interfaces
{
    public interface IUnixtimeStampService
    {
        public long Now { get; }
    }
}
