﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class UserEntity : BaseEntity
    {
        public string UserName { get; set; }
        public string UserAccountName { get; set; }
        public string UserPassword { get; set; }
        public long UserDateOfBirth { get; set; }
        public long UserEmail { get; set; }
    }
}
