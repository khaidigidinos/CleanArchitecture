﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Common
{
    public class BaseEntity
    {
        public string Id { get; set; }
        public long CreatedOn { get; set; }
    }
}
