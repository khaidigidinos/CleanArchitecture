﻿using Application.Common.Interfaces;
using Infra.Persistence;
using Infra.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infra.DependencyInjection
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfraDependencyInjection(this IServiceCollection services, IConfiguration configuration) 
        {
            /* Register services */
            services.AddTransient<IUnixtimeStampService, UnixtimeStampService>();
            services.AddTransient<IHashService, HashService>();
            /* Register ApplicationDbContext */
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    /* Migration purpose */
                    b => b.MigrationsAssembly(assemblyName: typeof(ApplicationDbContext).Assembly.FullName)
                );
            });
            /* Once per request */
            services.AddScoped<IApplicationDbContext>(propvider => propvider.GetRequiredService<ApplicationDbContext>());

            return services;
        }
    }
}
