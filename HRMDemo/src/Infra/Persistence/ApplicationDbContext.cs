﻿using Application.Common.Interfaces;
using Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infra.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        private readonly IUnixtimeStampService _unixtimeStampService;
        public ApplicationDbContext(IUnixtimeStampService unixtimeStampService) 
        {
            _unixtimeStampService = unixtimeStampService;
        }
        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) 
        {
            foreach (var entity in ChangeTracker.Entries<BaseEntity>())
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        entity.Entity.Id = Guid.NewGuid().ToString();
                        entity.Entity.CreatedOn = _unixtimeStampService.Now;
                        break;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
