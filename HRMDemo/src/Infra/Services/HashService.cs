﻿using Application.Common.Interfaces;
using System.Security.Cryptography;
using System.Text;

namespace Infra.Services
{
    public class HashService : IHashService
    {
        public string StringToSHA256(string hashNeeded)
        {
            SHA256 hash256 = SHA256.Create();
            byte[] hashBytes = hash256.ComputeHash(Encoding.UTF8.GetBytes(hashNeeded));
            var builder = new StringBuilder();
            foreach (var hByte in hashBytes) 
            {
                builder.Append(hByte.ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
