﻿using Application.Common.Interfaces;
using System;

namespace Infra.Services
{
    public class UnixtimeStampService : IUnixtimeStampService
    {
        public long Now => (long)((DateTime.Now.ToUniversalTime() - DateTime.UnixEpoch).Ticks / TimeSpan.TicksPerSecond);
    }
}
